const express = require('express');
var cors = require('cors')

const app = express();
const path = require('path');
const provinceRoute = require('./routes/province');
const env = require('dotenv').config()
const helmet = require('helmet')
app.use(cors())
app.use(helmet())
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile)
console.log("Environment", process.env.PORT)
console.log("Env", env.parsed)

app.use("*", function (req, res, next){
  //logger.info();
  next();
})

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/province", provinceRoute);
app.use("/province/search", provinceRoute)

app.use(express.json()) // rest api
app.use("/resource",express.static(path.resolve("./public"))) // static resource

app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});
app.listen(8080, function(){
  console.log('App listen to port 8080')
})