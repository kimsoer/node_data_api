
const connection = require('../db/index')
const router = require("express").Router();
router.get("/", function(req, res){
    connection.query('SELECT * FROM tbl_vulnerability as vb INNER JOIN tbl_commune as com on vb.COMMUNE_ID = com.COMMUNE_ID INNER JOIN Tbl_Year as year ON vb.YEAR_ID=year.YEAR_ID INNER JOIN tbl_district as di ON di.DISTRICT_ID = com.DISTRICT_ID INNER JOIN tbl_province as pr ON di.PROVINCE_ID=pr.PROVINCE_ID', function (err, rows, fields) {
      var data = {
        message: 'ok',
        data : rows
      }
      res.send(data)
    });
  
})
router.post("/search", function(req, res){

  var sql = 'SELECT * FROM tbl_vulnerability as vb INNER JOIN tbl_commune as com on vb.COMMUNE_ID = com.COMMUNE_ID INNER JOIN Tbl_Year as year ON vb.YEAR_ID=year.YEAR_ID INNER JOIN tbl_district as di ON di.DISTRICT_ID = com.DISTRICT_ID INNER JOIN tbl_province as pr ON di.PROVINCE_ID=pr.PROVINCE_ID WHERE com.COMMUNE_KH_NAME LIKE ?';
 connection.query(sql, [`%${req.body.commune}%`],  function (err, rows, fields) {
    var data = {
      message: 'ok',
      data : rows
    }
    res.send(data)
  });

})
router.post('/', function (req, res) {

 var sql  = `UPDATE tbl_vulnerability SET STORM = ?, DROUGHT = ?, COMPOSITE = ? , FLOOD = ? WHERE Vul_ID = ?`;
 connection.query(sql, [req.body.storm, req.body.drought, req.body.composite, req.body.flood, req.body.vulId])

  return res.send(req.body);

});

module.exports = router;